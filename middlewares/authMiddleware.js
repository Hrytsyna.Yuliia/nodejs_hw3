const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');


module.exports.authMiddleware = async (req, res, next) => {
  const header = req.headers['authorization'];
  if (!header) {
    return res
        .status(400)
        .json({message: `No Authorization http header found!`});
  }

  const token = header.substr(header.indexOf(' ')+1);

  if (!token) {
    return res.status(400).json({message: `Please log in!`});
  }

  req.user = jwt.verify(token, JWT_SECRET);
  next();
};
