const express = require('express');
const mongoose = require('mongoose');
const app = express();
const {PORT} = require('./config');

app.use(express.json());

const usersRouter = require('./routers/usersRouter');
const authRouter = require('./routers/authRouter');
const trucksRouter = require('./routers/trucksRouter');
const loadsRouter = require('./routers/loadsRouter');


app.use('/api/users/me', usersRouter);
app.use('/api/auth', authRouter);
app.use('/api/trucks', trucksRouter);
app.use('/api/loads', loadsRouter);

const start = async () => {
  await mongoose.connect('mongodb+srv://yuliia:yuliia@cluster0.bryei.mongodb.net/hw3?retryWrites=true&w=majority', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  app.listen(PORT, () => {
    console.log(`Server works at port ${PORT}!`);
  });
};

start();
