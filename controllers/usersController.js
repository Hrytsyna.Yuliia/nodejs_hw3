const {User} = require('../models/User');
const bcrypt = require('bcrypt');

module.exports.getProfileInfo = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});

  if (!user) {
    res
        .status(400)
        .json({message: `User with email ${req.user.email} not found!`});
  }

  res.status(200).json({message: 'Success', user: user});
};

module.exports.deleteProfile = async (req, res) => {
  const user = await User.findOne({_id: req.user._id});
  const userRole = req.user.role;

  if(userRole !== 'SHIPPER') {
    res.status(400).json({message: 'No permissions for this action!'});
  }

  if (!user) {
    res
        .status(400)
        .json({message: `User with email ${req.user.email} not found!`});
  }

  await User.findByIdAndDelete(req.user._id);
  res.status(200).json({message: 'Profile deleted successfully'});
};

module.exports.changePassword = async (req, res) => {
  const {oldPassword, newPassword} = req.body;
  if (!oldPassword || !newPassword) {
    res.status(400).json({message: `Please specify all parameters!`});
  };

  const newPass = await bcrypt.hash(newPassword, 10);
  await User.findByIdAndUpdate(req.user._id, {password: newPass});
  res.status(200).json({message: `Password updated! Please log in!`});
};
