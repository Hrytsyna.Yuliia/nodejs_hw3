const mongoose = require('mongoose');

const truckSchema = new mongoose.Schema({
  created_by: {
    type: String,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  type: {
    type: String,
  },
  height: {
    type: Number,
  },
  width: {
    type: Number,
  },
  depth: {
    type: Number,
  },
  payload: {
    type: Number,
  },
  status: {
    type: String,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Truck = mongoose.model('Truck', truckSchema);
