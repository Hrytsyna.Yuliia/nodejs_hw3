const {Truck} = require('../models/Truck');
const truckTypes = {};
truckTypes['SPRINTER'] = {
  weight: 300,
  height: 250,
  depth: 170,
  payload: 1700,
};
truckTypes['SMALL STRAIGHT'] = {
  weight: 500,
  height: 250,
  depth: 170,
  payload: 2500,
};
truckTypes['LARGE STRAIGHT'] = {
  weight: 700,
  height: 350,
  depth: 200,
  payload: 4000,
};

module.exports.getUserTrucks = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const trucks = await Truck.find({created_by: req.user._id});
  if (trucks.length === 0) {
    res.status(400).json({message: `No trucks found!`});
  }

  res.status(200).json({trucks: trucks});
};

module.exports.addUserTruck = async (req, res) => {
  const type = req.body.type;
  if (type) {
    const userRole = req.user.role;
    if (userRole !== 'DRIVER') {
      res.status(400).json({message: `No permissions for this action!`});
    }

    const truck = new Truck({
      created_by: req.user._id,
      type: type,
      weight: truckTypes[type].weight,
      height: truckTypes[type].height,
      depth: truckTypes[type].depth,
      payload: truckTypes[type].payload,
      status: 'IS',
    });
    await truck.save();
    res.status(200).json({message: 'New truck added!'});
  }

  res.status(400).json({message: `Please specify all parameters!`});
};

module.exports.getUserTruckById = async (req, res) => {
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const truck = await Truck.findOne({_id: req.params.id});
  if (!truck || truck.created_by !== req.user._id) {
    res
        .status(400)
        .json({message: `Truck with id ${req.params.id} not found!`});
  }

  res.status(200).json({truck: truck});
};

module.exports.updateUserTruckById = async (req, res) => {
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const truck = await Truck.findOne({_id: req.params.id});
  if (!truck) {
    res
        .status(400)
        .json({message: `Truck with id ${req.params.id} not found!`});
  }

  truck.type = req.body.type;
  truck.weight = truckTypes[req.body.type].weight;
  truck.height = truckTypes[req.body.type].height;
  truck.depth = truckTypes[req.body.type].depth;
  truck.payload = truckTypes[req.body.type].payload;
  await truck.save();

  res.status(200).json({message: 'Truck details changed successfully!'});
};

module.exports.deleteUserTruckById = async (req, res) => {
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const truck = await Truck.findOne({_id: req.params.id});
  if (!truck) {
    res
        .status(400)
        .json({message: `Truck with id ${req.params.id} not found!`});
  }

  await Truck.findByIdAndDelete(req.params.id);

  res.status(200).json({message: 'Truck deleted successfully!'});
};

module.exports.assignUserTruckById = async (req, res) => {
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const truck = await Truck.findOne({_id: req.params.id});
  if (!truck || truck.created_by !== req.user._id) {
    res
        .status(400)
        .json({message: `Truck with id ${req.params.id} not found!`});
  }

  truck['assigned_to'] = req.user._id;
  await truck.save();

  res.status(200).json({message: 'Truck assigned successfully!'});
};
