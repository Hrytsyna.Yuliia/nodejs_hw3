const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/User');

module.exports.registration = async (req, res) => {
  const {email, password, role} = req.body;

  const user = new User({
    email: email,
    password: await bcrypt.hash(password, 10),
    role: role.toUpperCase(),
  });

  await user.save();
  res.status(200).json({message: 'User created successfully!'});
};

module.exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    return res
        .status(400)
        .json({message: `No user with email '${email}' found!`});
  }

  if ( !(await bcrypt.compare(password, user.password)) ) {
    return res.status(400).json({message: `Wrong password!`});
  }

  const token = jwt
      .sign({email: user.email, _id: user._id, role: user.role}, JWT_SECRET);
  res.status(200).json({message: 'Success', jwt_token: token});
};

module.exports.forgotPassword = async (req, res) => {
  const {email} = req.body;
  const user = await User.findOne({email});

  if (!user) {
    res.status(400).json({message: `No user with email ${email} found!`});
  }

  res.status(200).json({message: `New password sent to your email address`});
};
