/**
 * @param {callback} callback some async function
 * @return {function}
 */
function asyncWrapper(callback) {
  return (req, res, next) => {
    callback(req, res, next)
        .catch(next);
  };
};

module.exports = {asyncWrapper};
