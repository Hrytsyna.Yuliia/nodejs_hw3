const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {asyncWrapper} = require('../helpers');
const {getProfileInfo,
  deleteProfile,
  changePassword,
} = require('../controllers/usersController');

router.get('/', authMiddleware, asyncWrapper(getProfileInfo));

router.delete('/', authMiddleware, asyncWrapper(deleteProfile));

router.patch('/password', authMiddleware, asyncWrapper(changePassword));

module.exports = router;
