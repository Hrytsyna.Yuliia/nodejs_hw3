const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {asyncWrapper} = require('../helpers');
const {getUserLoads,
  addUserLoad,
  getUserActiveLoad,
  iterateLoadState,
  getUserLoadById,
  updateUserLoadById,
  deleteUserLoadById,
  postUserLoadById,
  getUserLoadShippingDetailsById,
} = require('../controllers/loadsController');

router.get('/', authMiddleware, asyncWrapper(getUserLoads));

router.post('/', authMiddleware, asyncWrapper(addUserLoad));

router.get('/active', authMiddleware, asyncWrapper(getUserActiveLoad));

router.patch('/active/state', authMiddleware, asyncWrapper(iterateLoadState));

router.get('/:id', authMiddleware, asyncWrapper(getUserLoadById));

router.put('/:id', authMiddleware, asyncWrapper(updateUserLoadById));

router.delete('/:id', authMiddleware, asyncWrapper(deleteUserLoadById));

router.post('/:id/post', authMiddleware, asyncWrapper(postUserLoadById));

router.post('/:id/shipping_info', authMiddleware,
    asyncWrapper(getUserLoadShippingDetailsById));

module.exports = router;
