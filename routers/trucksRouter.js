const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {asyncWrapper} = require('../helpers');
const {getUserTrucks,
  addUserTruck,
  getUserTruckById,
  updateUserTruckById,
  deleteUserTruckById,
  assignUserTruckById,
} = require('../controllers/trucksController');

router.get('/', authMiddleware, asyncWrapper(getUserTrucks));

router.post('/', authMiddleware, asyncWrapper(addUserTruck));

router.get('/:id', authMiddleware, asyncWrapper(getUserTruckById));

router.put('/:id', authMiddleware, asyncWrapper(updateUserTruckById));

router.delete('/:id', authMiddleware, asyncWrapper(deleteUserTruckById));

router.post('/:id/assign', authMiddleware, asyncWrapper(assignUserTruckById));

module.exports = router;
