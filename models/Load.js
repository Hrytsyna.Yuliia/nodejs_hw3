const mongoose = require('mongoose');

const loadSchema = new mongoose.Schema({
  name: {
    type: String,
  },
  pickup_address: {
    type: String,
  },
  delivery_address: {
    type: String,
  },
  created_by: {
    type: String,
  },
  logs: {
    type: Array,
  },
  assigned_to: {
    type: String,
    default: null,
  },
  status: {
    type: String,
  },
  state: {
    type: String,
    default: '',
  },
  height: {
    type: Number,
  },
  dimensions: {
    type: Object,
  },
  payload: {
    type: Number,
  },
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports.Load = mongoose.model('Load', loadSchema);
