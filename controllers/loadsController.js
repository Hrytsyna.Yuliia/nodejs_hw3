const {Load} = require('../models/Load');
const {Truck} = require('../models/Truck');
const {User} = require('../models/User');

module.exports.getUserLoads = async (req, res) => {
  const userRole = req.user.role;
  let loads = [];
  const {skip=0, limit = 10} = req.query;
  const requestOptions = {
    skip: parseInt(skip),
    limit: limit > 50 ? 50 : parseInt(limit),
  };
  if (userRole === 'DRIVER') {
    loads = await Load.find({
      $and: [
        {assigned_to: req.user._id},
        {
          $or: [
            {status: 'ASSIGNED'},
            {status: 'SHIPPED'},
          ],
        },
      ],
    }).skip(requestOptions.skip).limit(requestOptions.limit);
  } else {
    loads = await Load
        .find({created_by: req.user._id})
        .skip(requestOptions.skip)
        .limit(requestOptions.limit);
  }

  if (loads.length === 0) {
    res.status(400).json({message: `No loads found!`});
  }

  res.status(200).json({loads: loads});
};

module.exports.addUserLoad = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'SHIPPER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const {name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = req.body;
  if (!name || !payload ||
    !pickup_address || !delivery_address || !dimensions) {
    res.status(400).json({message: `Please specify all parameters!`});
  }

  const load = new Truck({
    created_by: req.user._id,
    name: name,
    payload: payload,
    pickup_address: pickup_address,
    delivery_address: delivery_address,
    dimensions: dimensions,
    status: 'NEW',
    logs: ['NEW'],
  });

  await load.save();

  res.status(200).json({message: `Load created successfully!`});
};

module.exports.getUserActiveLoad = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }

  const load = await Load
      .findOne({assigned_to: req.user._id, status: 'ASSIGNED'});
  if (!load) {
    res.status(400).json({message: `No active load found!`});
  }

  res.status(200).json({load: load});
};

module.exports.iterateLoadState = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'DRIVER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const states = [
    'En route to Pick Up',
    'Arrived to Pick Up',
    'En route to delivery',
    'Arrived to delivery',
  ];
  const load = await Load.findOne({assigned_to: req.user._id});
  if (!load) {
    res.status(400).json({message: `No active load found!`});
  }

  const currentState = states.indexOf(load.state);
  load.state = states[currentState + 1];
  if (load.state === 'Arrived to delivery') {
    load.status = 'SHIPPED';
    load.logs.push('SHIPPED');
    const truck = await Truck.findOne({assigned_to: req.user._id});
    truck.status = 'IS';
    truck.assigned_to = null;
    await truck.save();
  }

  await load.save();

  res.status(200).json({message: `Load state changed to ${load.state}`});
};

module.exports.getUserLoadById = async (req, res) => {
  const load = await Load.findOne({_id: req.params._id});
  if (!load) {
    res.status(400).json({message: `No load found!`});
  }
  res.status(200).json({load: load});
};

module.exports.updateUserLoadById = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'SHIPPER') {
    res.status(400).json({message: `No permissions for this action!`});
  }
  const parameters = req.body;
  const load = await Load.findOne({_id: req.params._id});
  if (!load) {
    res.status(400).json({message: `No load found!`});
  }

  if (load.status !== 'NEW') {
    res.status(400).json({message: `Can't update assigned load!`});  
  }

  for (let key in parameters) {
    load[key] = parameters.key;
  }

  await load.save();
  res.status(200).json({message: `Load details changed successfully`});
};

module.exports.deleteUserLoadById = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'SHIPPER') {
    res.status(400).json({message: `No permissions for this action!`});
  }

  const load = await Load.findOne({_id: req.params.id});
  if (!load || load.created_by !== req.user._id) {
    res.status(400).json({message: `Load with id ${req.params.id} not found!`});
  }

  if (load.status !== 'NEW') {
    res.status(400).json({message: `Can't delete assigned load!`});
  }

  await Load.findByIdAndDelete(req.params.id);

  res.status(200).json({message: 'Load deleted successfully!'});
};

module.exports.postUserLoadById = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'SHIPPER') {
    res.status(400).json({message: `No permissions for this action!`});
  }

  const load = await Load.findOne({_id: req.params._id});
  if (!load) {
    res
        .status(400)
        .json({message: `Load with id ${req.params.id} not found!`});
  }

  if (load.status !== 'NEW') {
    res
        .status(400)
        .json({message: `Load already asigned or shipped!`});
  }

  load.status = 'POSTED';
  load.logs.push('POSTED');
  const availableTrucks = await Truck.find({status: 'IS'});
  if (availableTrucks.length === 0) {
    load.status = 'NEW';
    load.logs.push('NEW');
    load.save();
    res.status(400).json({message: `No available trucks found!`});
  }

  let result = false;
  let i = 0;
  while (!result && i < availableTrucks.length) {
    if (load.dimensions.width < availableTrucks[i].width &&
        load.dimensions.height < availableTrucks[i].heigh &&
        load.dimensions.depth < availableTrucks[i].depth) {
      const busyDriver = await User.find({_id: availableTrucks[i].created_by});
      if (!busyDriver) {
        const chosenTruck = await User.find({_id: availableTrucks[i]._id});
        chosenTruck.status = 'OL';
        chosenTruck.assigned_to = chosenTruck.created_by;
        load.assigned_to = chosenTruck._id;
        result = true;
      }
    }
    i++;
  }
  if (result) {
    res
        .status(200)
        .json({message: `Load posted successfully!`, driver_found: result});
  } else {
    res
        .status(200)
        .json({message: `No available trucks found!`, driver_found: result});
  }
};

module.exports.getUserLoadShippingDetailsById = async (req, res) => {
  const userRole = req.user.role;
  if (userRole !== 'SHIPPER') {
    res.status(400).json({message: `No permissions for this action!`});
  }

  const load = await Load.findOne({_id: req.params._id});
  if (!load || load.created_by !== req.user._id) {
    res
        .status(400)
        .json({message: `Load with id ${req.params.id} not found!`});
  }

  if (load.status === 'NEW') {
    res.status(400).json({message: `Load is not assigned to any truck!`});
  };

  const truck = Truck.findOne({_id: load.assigned_to});

  res.status(200).json({load: load, truck: truck});
};
